from app.internal.services.models.favorites_info import FavoritesInfo


def usernames_view(response: FavoritesInfo) -> str:
    return "; ".join([fav.username for fav in response.result])
