from django.contrib import admin

from app.internal.models import AdminUser, BankAccount, BankCard, TelegramUser


@admin.register(TelegramUser)
class TelegramUserAdmin(admin.ModelAdmin):
    list_display = ("telegram_id", "username", "first_name", "last_name", "phone")


@admin.register(BankAccount)
class BankAccountAdmin(admin.ModelAdmin):
    list_display = ("get_client_telegram_id", "number", "amount")

    def get_client_telegram_id(self, obj):
        return obj.client.telegram_id

    get_client_telegram_id.short_description = "Client id"


@admin.register(BankCard)
class BankCardAdmin(admin.ModelAdmin):
    list_display = ("number", "expiration_date", "get_account_number", "get_account_amount")

    def get_account_number(self, obj):
        return obj.account.number

    def get_account_amount(self, obj):
        return obj.account.amount

    get_account_number.short_description = "Account number"
    get_account_amount.short_description = "Account amount"
