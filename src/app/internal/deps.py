from app.internal.services.bot_manager import BotManager
from app.internal.services.jwt_token_generator import JWTGenerator
from app.internal.services.storage.bank_storage import BankStorage
from app.internal.services.storage.favorites_storage import FavoritesStorage
from app.internal.services.storage.token_storage import TokenStorage
from app.internal.services.storage.user_storage import UserStorage

user_storage = UserStorage()
bot_manager = BotManager(user_storage, FavoritesStorage(), BankStorage())
token_storage = TokenStorage()
jwt_generator = JWTGenerator(token_storage)
