from typing import Optional

from pydantic.types import SecretStr
from telegram import User

from app.internal.models import BankAccount, TelegramUser


class UserStorage:
    def get_user_by_name(self, name: str) -> Optional[TelegramUser]:
        try:
            return TelegramUser.objects.get(username=name)
        except TelegramUser.DoesNotExist:
            return None

    def get_user(self, telegram_id: int) -> Optional[TelegramUser]:
        try:
            return TelegramUser.objects.get(telegram_id=telegram_id)
        except TelegramUser.DoesNotExist:
            return None

    def create_user(self, user: User):
        return TelegramUser.objects.create(
            telegram_id=user.id, first_name=user.first_name, last_name=user.last_name, username=user.username
        )

    def set_phone(self, user: TelegramUser, phone: str) -> None:
        user.phone = phone
        user.save(update_fields=["phone"])

    def get_account_number(self, user: TelegramUser) -> Optional[str]:
        return BankAccount.objects.filter(client=user).values_list("number", flat=True).first()

    def set_password(self, user: TelegramUser, password: SecretStr) -> bool:
        try:
            user.set_password(password.get_secret_value())
            user.save(update_fields=["password"])
        except Exception as ex:
            print(ex)
            return False

        return True
