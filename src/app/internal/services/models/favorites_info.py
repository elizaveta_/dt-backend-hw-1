from enum import Enum
from typing import TypeVar, Union

from pydantic import BaseModel

from app.internal.services.models.user_info import TelegramUserModel


class FavoritesStatus(Enum):
    FIND_FAVORITES = "find_favorites"
    NOT_FAVORITES = "not_favorites"
    ADD_FAVORITES = "add_favorites"
    DELETE_FAVORITES = "delete_favorites"


class FavoritesErrorStatus(Enum):
    NOT_ALLOWED_SELF = "not_allowed_self"
    MISSING_ARGUMENTS = "MISSING_ARGUMENTS"


FavoritesResponseStatus = TypeVar("FavoritesResponseStatus", FavoritesStatus, FavoritesErrorStatus)


class FavoritesInfo(BaseModel):
    status: FavoritesResponseStatus

    result: list[TelegramUserModel] = []
