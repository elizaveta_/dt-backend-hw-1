from enum import Enum
from typing import Optional

from pydantic import BaseModel


class TelegramUserInfoModel(BaseModel):
    telegram_id: int
    first_name: str
    last_name: Optional[str]
    username: str
    phone: Optional[str]


class TelegramUserModel(TelegramUserInfoModel):
    favorites: list[TelegramUserInfoModel] = []


class PhoneStatus(Enum):
    NOT_CORRECT_PHONE = "not_correct_phone"
    UPDATE_PHONE = "update_phone"


class UserStatus(Enum):
    NEW = "new"
    OLD = "old"


class TelegramUserResponse(BaseModel):
    status: UserStatus
    user: TelegramUserModel
