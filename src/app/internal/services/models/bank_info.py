import datetime
from datetime import date
from enum import Enum
from typing import Optional

from pydantic import BaseModel

from app.internal.models.transaction import Transaction
from app.internal.services.models.user_info import TelegramUserModel


class BankAccountModel(BaseModel):
    client: TelegramUserModel
    amount: float
    number: str


class BankCardModel(BaseModel):
    number: str
    expiration_date: date
    security_code: int
    account: "BankAccountModel"


class BankInfo(BaseModel):
    account: Optional[BankAccountModel] = None
    cards: list[BankCardModel] = []


class TransactionInfoStatus(Enum):
    NOT_TRANSACTION = "not_transaction"
    NOT_ACCOUNT = "not_account"
    FIND_TRANSACTION = "FIND_TRANSACTION"


class TransactionModel(BaseModel):
    from_: str
    to: str
    amount: str
    created_at: datetime.datetime


class TransactionInfo(BaseModel):
    status: TransactionInfoStatus
    transactions: list[TransactionModel] = []
