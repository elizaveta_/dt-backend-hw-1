from http import HTTPStatus

from django.http import JsonResponse
from ninja import NinjaAPI, Schema

from app.internal.deps import jwt_generator, token_storage, user_storage
from app.internal.models import TelegramUser
from app.internal.transport.rest.auth import Authentication

api = NinjaAPI()


class LoginScheme(Schema):
    username: str
    password: str


def serialize_user(user: TelegramUser) -> dict[str, str]:
    return {
        "telegram_id": user.telegram_id,
        "first_name": user.first_name,
        "last_name": user.last_name,
        "username": user.username,
    }


@api.post("/login")
def login(request, data: LoginScheme):
    user = user_storage.get_user_by_name(data.username)
    if not user or not user.check_password(data.password):
        return JsonResponse({"detail": "Unauthorized"}, status=HTTPStatus.UNAUTHORIZED)

    return jwt_generator.generate_access_and_refresh_tokens(user)


@api.post("/update_tokens")
def update_tokens(request, refresh_token: str):
    if token := token_storage.get_token(refresh_token):
        return jwt_generator.generate_access_and_refresh_tokens(token.user)


@api.post("/me", auth=Authentication())
def me(request):
    if (token := request.auth) is None:
        return JsonResponse({"detail": "Token is not correct"}, status=HTTPStatus.FORBIDDEN)

    telegram_id = jwt_generator.get_telegram_id(token)
    if (user := user_storage.get_user(telegram_id)) is None:
        return JsonResponse({"detail": "User not found"}, status=HTTPStatus.NOT_FOUND)

    return serialize_user(user)
