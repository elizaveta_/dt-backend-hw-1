def mocked_manager_handler(response):
    def func(*args):
        return response

    return func
