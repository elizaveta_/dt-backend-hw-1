import pytest

from app.internal.models import BankAccount, TelegramUser
from app.internal.models.transaction import Transaction
from app.internal.services.storage.bank_storage import BankStorage
from app.internal.services.storage.favorites_storage import FavoritesStorage
from app.internal.services.storage.token_storage import TokenStorage
from app.internal.services.storage.user_storage import UserStorage


@pytest.fixture(params=[])
def add_user_in_database(request):
    TelegramUser.objects.bulk_create(
        TelegramUser(
            telegram_id=user.telegram_id, first_name=user.first_name, last_name=user.last_name, username=user.username
        )
        for user in request.param
    )


@pytest.fixture
def user_storage():
    return UserStorage()


@pytest.fixture
def favorites_storage():
    return FavoritesStorage()


@pytest.fixture
def bank_storage():
    return BankStorage()


@pytest.fixture
def token_storage():
    return TokenStorage()


@pytest.fixture()
def test_db_user(faker):
    return TelegramUser.objects.create(
        telegram_id=faker.random_int(min=1), first_name=faker.word(), last_name=faker.word(), username=faker.word()
    )


@pytest.fixture()
def second_test_db_user(faker):
    return TelegramUser.objects.create(
        telegram_id=faker.random_int(), first_name=faker.word(), last_name=faker.word(), username=faker.word()
    )


@pytest.fixture
def test_user_with_account(faker, test_db_user):
    BankAccount.objects.create(client=test_db_user, amount=float(faker.random_int(min=1)), number=faker.random_int())
    return test_db_user


@pytest.fixture
def second_test_user_with_account(faker, second_test_db_user):
    BankAccount.objects.create(
        client=second_test_db_user, amount=float(faker.random_int(min=1)), number=faker.random_int()
    )
    return second_test_db_user


@pytest.fixture
def create_transactions(faker, test_user_with_account, second_test_user_with_account):
    Transaction.objects.create(
        from_account=test_user_with_account.account,
        to_account=second_test_user_with_account.account,
        transaction_amount=faker.random_int(),
    )
    Transaction.objects.create(
        from_account=second_test_user_with_account.account,
        to_account=test_user_with_account.account,
        transaction_amount=faker.random_int(),
    )
    Transaction.objects.create(
        from_account=test_user_with_account.account,
        to_account=second_test_user_with_account.account,
        transaction_amount=faker.random_int(),
    )
