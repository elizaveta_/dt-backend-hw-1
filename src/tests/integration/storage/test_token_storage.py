import pytest

TOKEN = "kek"


@pytest.mark.django_db
class TestTokenStorage:
    def test_empty_storage(self, token_storage):
        assert not token_storage.get_token(TOKEN)

    def test_create_token(self, token_storage, test_db_user):
        assert not token_storage.get_token(TOKEN)

        token_storage.create_token(test_db_user, TOKEN)

        actual = token_storage.get_token(TOKEN)
        assert actual.user == test_db_user
        assert actual.jwt == TOKEN
        assert actual.revoked is False
