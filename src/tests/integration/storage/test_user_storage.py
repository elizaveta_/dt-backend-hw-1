import pytest

from app.internal.models import BankAccount, TelegramUser


@pytest.mark.django_db
class TestUserStorage:
    def test_get_and_create_user(self, user_storage, user):
        assert not user_storage.get_user(12345)

        user_storage.create_user(user)

        assert user_storage.get_user(user.id) == TelegramUser.objects.get(telegram_id=user.id)

    def test_get_user_by_name(self, user_storage, user):
        assert not user_storage.get_user_by_name(user.username)

        user_storage.create_user(user)

        assert user_storage.get_user_by_name(user.username) == TelegramUser.objects.get(username=user.username)

    def test_set_phone(self, user_storage, test_db_user):
        user_storage.set_phone(test_db_user, "+79123456789")

        assert user_storage.get_user_by_name(test_db_user.username).phone == "+79123456789"

    def test_get_acoount_number(self, user_storage, test_user_with_account):
        assert user_storage.get_account_number(user=test_user_with_account) == str(
            test_user_with_account.account.number
        )
